"use strict";

// сделать все карточки hidden
function hideCards() {
    let cards = document.querySelectorAll('[data-category]');
    cards.forEach(function (card){
        card.setAttribute('hidden','hidden');
    })
}

// сделать все карточки видимыми - show all
function showCards() {
    let cards = document.querySelectorAll('[data-category]');
    cards.forEach(function (card){
        card.removeAttribute('hidden');
    })
}

// сортировка по имени data-атрибута и его значению
// attribute - строка, может принимать значение 'category', 'color', 'size', 'price'
// value - значение атрибута для конкретной карточки (приходит из БД)
function sortCards(attribute, value){
    hideCards();
    let selectedCards = document.querySelectorAll(`[data-${attribute}='${value}']`);
    selectedCards.forEach(function (card){
        card.removeAttribute('hidden');
    })
}