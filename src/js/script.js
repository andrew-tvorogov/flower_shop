"use strict";

console.log('script js is loaded');

// запрос на добавление bouquet в favorites
function favoritesAddRemove(bouquetId, userId) {
    let answer = '';
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            answer += this.responseText;
            //console.log(answer);
            location.reload();
            //cardInFavoritesExistCheck();
        }
    };
    xhr.open('GET', `favorites_add_remove.php?bouquet_id=${bouquetId}&user_id=${userId}`, true);
    xhr.send();
}

// TODO: реализовать реакцию карточки без перегрузки страницы
// реакция карточки на существование букета в favorites
function cardInFavoritesExistCheck() {
    if (answer === '1') {
        console.log('bouquet exists in favorites');
        // signupEmailLable.innerHTML = "Email exists";
        // signupEmail.classList.remove('is-valid');
        // signupEmail.classList.add('is-invalid');
        // signupEmailLable.style.color = "#cc223b";
        return false;
    } else if (answer === '0') {
        // signupEmailLable.innerHTML = "Email";
        // signupEmail.classList.remove('is-invalid');
        // signupEmail.classList.add('is-valid');
        // signupEmailLable.style.color = "#643939";
        console.log('bouquet does not exist in favorites');
        return true;
    }
}