"use strict";

showCartItemQuantity(getCartItemQuantity());

function getCartItemQuantity(){
    let quantity = 0;
    const currentCartString = getCookie('cart');
    const currentCart = (currentCartString === '') ? {} : JSON.parse(currentCartString); // { 2: 5 }
    for (let key in currentCart) {
        quantity += currentCart[key];
    }
    console.log('Количество в корзине: ' + quantity);
    return quantity;
}

function addToCart(id) {
    const currentCartString = getCookie('cart');
    const currentCart = (currentCartString === '') ? {} : JSON.parse(currentCartString); // { 2: 5 }
    if (currentCart.hasOwnProperty(id)) {
        currentCart[id] += 1;
    } else {
        currentCart[id] = 1;
    }
    setCookie('cart', JSON.stringify(currentCart), {
        path: '/'
    });
    showCartItemQuantity(getCartItemQuantity());
    location.reload();
}

function removeFromCart(id) {
    const currentCartString = getCookie('cart');
    const currentCart = (currentCartString === '') ? {} : JSON.parse(currentCartString); // { 2: 5 }
    if (currentCart.hasOwnProperty(id) && currentCart[id] > 0) {
        currentCart[id] -= 1;
    } else {
        currentCart[id] = 0;
    }
    setCookie('cart', JSON.stringify(currentCart), {
        path: '/'
    });
    showCartItemQuantity(getCartItemQuantity());
    location.reload();
}

function deleteFromCart(id) {
    const currentCartString = getCookie('cart');
    const currentCart = (currentCartString === '') ? {} : JSON.parse(currentCartString); // { 2: 5 }
    if (currentCart.hasOwnProperty(id)) {
        delete currentCart[id];
    }
    setCookie('cart', JSON.stringify(currentCart), {
        path: '/'
    });
    showCartItemQuantity(getCartItemQuantity());
    if (getCartItemQuantity() === 0){
        deleteCookie('cart', '/');
    }
    location.reload();
}

function showCartItemQuantity(num) {
    let quantity = num;
    if (document.querySelector('#cart_items_counter_view')){
        document.querySelector('#cart_items_counter_view').innerHTML = quantity;
    }
}

