<?php
    include_once('globals.php'); //
    include_once('functions.php');
    include_once('bouquets.php'); // букеты из bd
    include_once('florists.php'); // флористы из bd
    include_once('orders.php'); // заказы из bd
    //include_once('cart.php'); // cart из bd


// получить данные из формы
$delivery_id = $_POST['delivery_type_id'];
$user_id = $_POST['user_id'];
if (!$user_id) {
    $user_id = 1; // anonymouse id 1
}
$user_name = htmlentities($_POST['user_name']);
$user_phone = htmlentities($_POST['user_phone']);
$delivery_address = htmlentities($_POST['delivery_address']);
$comment = htmlentities($_POST['comment']);

// корзина из куки
$bouquetsCart = [];

if (isset($_COOKIE['cart'])) {
    $cart = json_decode($_COOKIE['cart'], true);
//    echo "<pre>";
//    print_r($cart);
//    echo "</pre>";
    $ids = array_keys($cart);
    $bouquetsCartInfo = getCartInfo($ids);
    foreach ($bouquetsCartInfo as &$bouquetCart) {
        $bouquetCart['bouquet_quantity'] = $cart[$bouquetCart['bouquet_id']];
    }
    $bouquetsCart = $bouquetsCartInfo;
}

function getCartInfo($ids)
{
    $idsString = implode(',', $ids);
    $db = getConnection();
    $query = "
    SELECT `bouquet_id` 
    FROM `bouquets`
    WHERE `bouquet_id` IN ($idsString);
    ";
    $result = mysqli_query($db, $query);
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

    // создать order
$db = getConnection();
$query = "
INSERT INTO `orders` (`order_delivery_id`, `order_user_id`, `order_delivery_address`, `order_delivery_comment`, `order_user_name`, `order_user_phone`) 
VALUES ($delivery_id, $user_id, '$delivery_address', '$comment', '$user_name', '$user_phone');
";
mysqli_query($db, $query);
$order_id = mysqli_insert_id($db); // id нового order'a
//echo $order_id;

// создание set'ов к order
foreach ($bouquetsCart as $set) {
    $bouquet_id = $set['bouquet_id'];
    $bouquet_quantity = $set['bouquet_quantity'];
    $query = "
            INSERT INTO `sets` (`set_bouquet_id`, `set_order_id`, `set_count`) 
            VALUES ( $bouquet_id, $order_id, $bouquet_quantity);
            ";
    //echo $query;
    mysqli_query($db, $query);
}

// TODO: clean cart
if ($user_id != 1) {
    setcookie("cart", '', (time() - 3600), '/'); // удаление куков корзины
    // TODO: сообщение об успешном создании заказа
    include_once('./templates/header.html');
    include_once('./templates/login_modal.html');
    include_once('./templates/header_logo.html');
    include_once('./templates/navigation.html');

    include_once('./templates/message.html');

    include_once('./templates/footer.html');
    //header("Location: cabinet.php");
} else {
    setcookie("cart", '', (time() - 3600), '/'); // удаление куков корзины
    // TODO: сообщение об успешном создании заказа

    include_once('./templates/header.html');
    include_once('./templates/login_modal.html');
    include_once('./templates/header_logo.html');
    include_once('./templates/navigation.html');

    include_once('./templates/message.html');

    include_once('./templates/footer.html');
    //header("Location: index.php");
}


?>
