<?php

include_once('functions.php');
include_once('globals.php'); // в globals лежит значение customer_id
include_once('bouquets.php'); // букеты из bd
include_once('florists.php'); // флористы из bd
include_once('users.php'); // информация о юзерах
include_once('sets.php'); // наборы из bd
include_once('deliveries.php'); // типы доставки

// замещает закомментированный код ниже
include_once('cart_data.php'); // данные о корзине из БД

//// корзина из куки
//function getCartInfo($ids)
//{
//    $idsString = implode(',', $ids);
//    $db = getConnection();
//    $query = "
//    SELECT *
//    FROM `bouquets`
//    WHERE `bouquet_id` IN ($idsString);
//    ";
//    $result = mysqli_query($db, $query);
//    return mysqli_fetch_all($result, MYSQLI_ASSOC);
//}
//
//$bouquetsCart = [];
//if (isset($_COOKIE['cart'])) {
//    $cart = json_decode($_COOKIE['cart'], true);
//    $ids = array_keys($cart);
//    $bouquetsCartInfo = getCartInfo($ids);
//    foreach ($bouquetsCartInfo as &$bouquetCart) {
//        $bouquetCart['bouquet_quantity'] = $cart[$bouquetCart['bouquet_id']];
//    }
//    $bouquetsCart = $bouquetsCartInfo;
//}

//echo "<pre>";
//print_r($bouquetsCart);
//echo "</pre>";

//$summ = 0;
//foreach ($bouquetsCart as $orderSumm) {
//    $summ = $summ + ($orderSumm['bouquet_price'] * $orderSumm['bouquet_quantity']);
//}

// убрал, добавил непосредственно в html шаблон
// иначе трудно разобраться в коде
//if (isset($_COOKIE['user_id'])){
    //$user = getUserInfo($_COOKIE['user_id']);
//}


include_once('./templates/header.html');
include_once('./templates/login_modal.html');
include_once('./templates/header_logo.html');
include_once('./templates/navigation.html');

include_once('./templates/cart.html');

include_once('./templates/footer.html');
?>