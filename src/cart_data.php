<?php

// достаёт данные по лежащим в корзине (в куках) букетам
function getCartInfo($ids)
{
    $idsString = implode(',', $ids);
    $db = getConnection();
    $query = "
    SELECT * 
    FROM `bouquets`
    WHERE `bouquet_id` IN ($idsString);
    ";
    $result = mysqli_query($db, $query);
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

// корзина из куки
$bouquetsCart = [];
if (isset($_COOKIE['cart'])) {
    $cart = json_decode($_COOKIE['cart'], true);
    $ids = array_keys($cart);
    $bouquetsCartInfo = getCartInfo($ids);
    foreach ($bouquetsCartInfo as &$bouquetCart) {
        $bouquetCart['bouquet_quantity'] = $cart[$bouquetCart['bouquet_id']];
    }
    $bouquetsCart = $bouquetsCartInfo;
}
// $bouquetsCart
//
// массив данных по каждому букету из БД с добавленным свойством bouquet_quantity
// формат данных:
//Array
//(
//    [0] => Array
//    (
//            [bouquet_id] => 36
//            [bouquet_name] => Meganame
//            [bouquet_price] => 25
//            [bouquet_img] => img_5.png
//            [bouquet_views] => 0
//            [bouquet_likes] => 0
//            [bouquet_descr] => This is so irreverent bouquets, you never know when it will be needed
//            [bouquet_florist_id] => 3
//            [bouquet_color_id] => 2
//            [bouquet_size_id] => 1
//            [bouquet_category_id] => 0
//            [bouquet_is_deleted] => 0
//            [bouquet_quantity] => 1
//        )
//)

// есть ли в массиве $bouquets букеты с $id
// нужно для вывода количества букетов на кнопку 'add to cart'
function bouquetExistInCart($id, $bouquets) {
    foreach ($bouquets as $bouquet) {
        if ($id === $bouquet['bouquet_id']) {
            return true;
        }
    }
    return false;
}

// $summ
//
// общая сумма заказа
$total = 0;
foreach ($bouquetsCart as $orderTotal) {
    $total = $total + ($orderTotal['bouquet_price'] * $orderTotal['bouquet_quantity']);
}

//echo "<pre>";
//print_r($bouquetsCart);
//echo "</pre>";