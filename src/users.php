<?php

// getUserInfo($id) возвращает один элемент со свойствами user'a
// в формате:
//Array
//(
//    [user_id] => 3
//    [user_phone] => 1(111)3569855
//    [user_name] => John Travolta
//    [user_email] => 2@2
//    [user_password] => c81e728d9d4c2f636f067f89cc14862c
//    [user_create_time] => 2020-12-28 22:26:10
//    [user_address_id] => 6
//    [user_is_deleted] => 0
//    [user_is_admin] => 0
//    [address_id] => 6
//    [address_city] => Scranton
//    [address_street] => Summer Lane
//    [address_building] => 2b
//    [address_apartment] => 14
//    [address_floor] => 1
//)
//
function getUserInfo($id)
{
    $db = getConnection();
    $query = "SELECT * 
              FROM `users`
              LEFT JOIN `addresses` ON `user_address_id` = `address_id`
              WHERE `user_id` = $id";
    $result = mysqli_query($db, $query);
    $userInfo = mysqli_fetch_assoc($result);
    return $userInfo;
}

//function getUserInfo($id)
//{
//    $db = getConnection();
//    $query = "SELECT *
//              FROM `users`
//              LEFT JOIN `addresses` ON `user_address_id` = `address_id`
//              WHERE `user_id` = $id";
//    $result = mysqli_query($db, $query);
//    $userInfo = mysqli_fetch_all($result, MYSQLI_ASSOC);
//    return $userInfo;
//}

function getUserOrders($id)
{
    $db = getConnection();
    $query = "
		SELECT `order_id`, `delivery_name` AS `order_delivery_name`, `status_name` AS `order_status_name`, 
		       `user_name` AS `order_user_name`, `user_id`, `bouquet_name`, `bouquet_img`, `order_start_time`			
		FROM `orders`
        LEFT JOIN `deliveries` ON `delivery_id` = `order_delivery_id`
        LEFT JOIN `users` ON `order_user_id` = `user_id`
        LEFT JOIN `statuses` ON `order_status_id` = `status_id` 
		LEFT JOIN `sets` ON `set_order_id` = `order_id`
		LEFT JOIN `bouquets` ON `set_bouquet_id` = `bouquet_id`
        WHERE `order_user_id` = '$id';
	";
    $result = mysqli_query($db, $query);
    $orders = mysqli_fetch_all($result, MYSQLI_ASSOC);
    return $orders;
}
