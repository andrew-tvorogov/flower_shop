<?php
    include_once('functions.php');
    include_once('globals.php'); // в globals лежит значение customer_id
    include_once('florists.php'); // флористы из bd
    //include_once('cart_data.php'); // корзина - информация по букетам в корзине

    include_once('./templates/header.html');
    include_once('./templates/login_modal.html');
    include_once('./templates/header_logo.html');
    include_once('./templates/navigation.html');

if (isset($_GET['delivery_areas'])){
    include_once('./templates/footer/delivery_areas.html');
}
if (isset($_GET['careers'])){
    include_once('./templates/footer/careers.html');
}
if (isset($_GET['history'])){
    include_once('./templates/about.html');
}
if (isset($_GET['sitemap'])){
    include_once('./templates/footer/sitemap.html');
}

if (isset($_GET['substitution'])){
    include_once('./templates/footer/substitution.html');
}
if (isset($_GET['refund'])){
    include_once('./templates/footer/refund.html');
}
if (isset($_GET['suggestions'])){
    include_once('./templates/footer/suggestions.html');
}

if (isset($_GET['privacy'])){
    include_once('./templates/footer/privacy.html');
}
if (isset($_GET['terms'])){
    include_once('./templates/footer/terms.html');
}


    include_once('./templates/footer.html');
?>

