<?php
include_once('globals.php'); //
include_once('functions.php');
include_once('bouquets.php'); // букеты из bd

if (isAdmin()) {
    if (isset($_GET['delete'])){
        $id = htmlentities($_GET['delete']);
        $db = getConnection();
        $query = "UPDATE `bouquets` SET
                    `bouquet_is_deleted` = '1'                                        
                    WHERE `bouquet_id` = '$id';
                    ";
        mysqli_query($db, $query);
        header('Location: admin.php');
    }
    header('Location: admin.php');
} else {
    //error_reporting(0);
    header("Location: index.php");
}
?>