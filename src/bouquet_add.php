<?php

include_once('globals.php'); //
include_once('functions.php');
include_once('bouquets.php'); // букеты из bd
include_once('florists.php'); // флористы из bd

if (isAdmin()) {

    // список имён файлов изображений получен в bouquets.php

    // проверяем пришли ли данные формы, если пришли - идём на страницу admin-bouquets
    if (isset($_POST['name'])) {
        $bouquet_name = htmlentities($_POST['name']);
        $bouquet_price = htmlentities($_POST['price']);
        $bouquet_image = htmlentities($_POST['image']);
        $bouquet_description = htmlentities($_POST['description']);
        $bouquet_florist_id = htmlentities($_POST['florist_id']);
        $bouquet_color_id = htmlentities($_POST['color_id']);
        $bouquet_size_id = htmlentities($_POST['size_id']);
        $bouquet_category_id = htmlentities($_POST['category_id']);
        $db = getConnection();
        $query = "INSERT INTO `bouquets`
                SET `bouquet_name` = '$bouquet_name',
                    `bouquet_price` = '$bouquet_price',
                    `bouquet_img` = '$bouquet_image',
                    `bouquet_descr` = '$bouquet_description',
                    `bouquet_florist_id` = '$bouquet_florist_id',
                    `bouquet_color_id` = '$bouquet_color_id',
                    `bouquet_size_id` = '$bouquet_size_id',
                    `bouquet_category_id` = '$bouquet_category_id';
        ";
        mysqli_query($db, $query);
        //error_reporting(0);

        header('Location: admin.php');
    } else {
        include_once('./templates/header.html');
        include_once('./templates/header_logo.html');
        include_once('./templates/navigation.html');
        include_once('./templates/bouquet_add.html');
        include_once('./templates/footer.html');
    }
} else {
    //error_reporting(0);
    header("Location: index.php");
}
?>