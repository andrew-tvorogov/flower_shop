<?php

include_once('globals.php'); // в globals лежит значние customer_id
include_once('functions.php');
//include_once('florists.php'); // флористы из bd

//include_once('./templates/header.html');
//include_once('./templates/login_modal.html');
//include_once('./templates/header_logo.html');
//include_once('./templates/navigation.html');

$isAuthorized = isAuthorized();
$isAdmin = isAdmin();

if (!$isAuthorized) {
    // если передан email
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];

        if ($password == '' || $email == '') {
            echo '<div class="container text-danger"><b>Внимание: не все поля заполнены!</b></div>';
        } else {
            $hash_password = md5($password);
            $db = getConnection();
            $query = "
					SELECT `user_id`, `user_name`, `user_email`, `user_is_admin`
					FROM `users`
					WHERE `user_email` = '$email'
					AND `user_password` = '$hash_password';
				";
            $result = mysqli_query($db, $query);

            if (mysqli_num_rows($result) == 1) {
                $user = mysqli_fetch_assoc($result);
                $user_id = $user['user_id'];
                $user_name = $user['user_name'];
                $admin = $user['user_is_admin'];

                // TODO: зачем тут session?
                // на всякий случай в базу пишу, но надо разобраться
                session_start();
                $session_id = session_id();

                $token = generateToken();
                $token_time = time() + 60*15;
                setcookie('token_time', $token_time, time() + 2*24*3600, '/');
                setcookie('token', $token, time() + 2*24*3600, '/');
                setcookie('user_id', $user_id, time() + 2*24*3600, '/');
                setcookie('user_name', $user_name, time() + 2*24*3600, '/');
                setcookie('admin', $admin, time() + 2*24*3600, '/');

                $query = "
						INSERT INTO `connects`
						SET `connect_token` = '$token',
						`connect_user_id` = '$user_id',
						`connect_session_id` = '$session_id',
						`connect_token_time` = FROM_UNIXTIME($token_time);
					";
                mysqli_query($db, $query);
                mysqli_close($db);

                header('Location: index.php');
            } else {
                //echo 'Нет такой связки логин/пароль';
                //header('Location: error.php');
                header('Location: error.php?err=Pair login\\password  - doesn\'t exist');
                die;
            }
        }
    }
    // email _не_ передан идём на главную
    // можно сделать сообщение
    header('Location: index.php');
} else {
    // авторизирован идём на главную
    header('Location: index.php');
}

?>