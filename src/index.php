<?php
    include_once('functions.php');
    include_once('globals.php'); // в globals лежит значение customer_id
    include_once('bouquets.php'); // букеты из bd

    include_once ('favorites_add_remove.php'); // проверяем favorites

    include_once('florists.php'); // флористы из bd

    include_once('cart_data.php'); // корзина - информация по букетам в корзине

    include_once('./templates/header.html');
    include_once('./templates/login_modal.html');
    include_once('./templates/header_logo.html');
    include_once('./templates/navigation.html');
    include_once('./templates/banner.html');
    include_once('./templates/sort.html');

    include_once('./templates/cards.html');

    include_once('./templates/footer.html');
?>

