<?php

include_once('globals.php'); // в globals лежит значние customer_id
include_once('functions.php');
include_once('bouquets.php'); // букеты из bd
include_once('florists.php'); // флористы из bd
include_once('orders.php'); // заказы из bd
include_once('users.php'); // информация о юзерах
include_once('favorites_add_remove.php'); // favorites

if (isset($_POST['user_id']) && isset($_POST['phone']) && isset($_POST['name'])) {

    $uId = $_POST['user_id'];
    $uPhone = $_POST['phone'];
    $uName = $_POST['name'];

    $db = getConnection();
    $query = "UPDATE `users` 
                 SET `user_name` = '$uName',
                     `user_phone` = '$uPhone'
               WHERE `user_id` = $uId";
    mysqli_query($db, $query);

    //header_remove();

    setcookie('user_name', $uName, time() + 2*24*3600, '/');
    header('Location: cabinet.php');
}

include_once('./templates/header.html');
include_once('./templates/header_logo.html');
include_once('./templates/navigation.html');

$userId = $_COOKIE['user_id'];
$user = getUserInfo($userId); // функция в users.php
$orders = getUserOrders($userId); // функция в users.php
$favorites = getFavorites($userId); // функция в favorites_add_remove.php

include_once('./templates/cabinet.html');
include_once('./templates/footer.html');