<?php
// список изображений букетов
$dir    = 'img/bouquets';
$images = scandir($dir);
for ($i = 0; $i < count($images); $i++ ){
    if($images[$i] == '.' || $images[$i] == '..' ){
        unset($images[$i]);
    }
}
//    echo "<pre>";
//    print_r($images);
//    echo "</pre>";
//    die;

// сортировка по цене! TODO: сделать переключение сортировки
$connect = getConnection();
$query = "
		SELECT `bouquet_id` AS `id`, `bouquet_name`, `bouquet_price`, `bouquet_views`,
			`bouquet_likes`, `bouquet_img`, `bouquet_descr`, `bouquet_is_deleted`, 
		    `florist_name`,
		    `bouquet_category_id`, `category_name`,
		    `bouquet_size_id`, `size_name`,
		    `bouquet_color_id`, `color_name`
		FROM `bouquets`		
		LEFT JOIN `florists` ON `bouquet_florist_id` = `florist_id`
		LEFT JOIN `categories` ON `bouquet_category_id` = `category_id`
		LEFT JOIN `sizes` ON `bouquet_size_id` = `size_id`
		LEFT JOIN `colors` ON `bouquet_color_id` = `color_id`
		WHERE `bouquet_is_deleted` = 0
        ORDER BY `bouquet_price`;
	";
$result = mysqli_query($connect, $query);
$bouquets = mysqli_fetch_all($result, MYSQLI_ASSOC);


// получаем всё из таблицы colors
$query = "
		SELECT *
		FROM `colors`
        ORDER BY `color_id`;        
	";
$result = mysqli_query($connect, $query);
$colors = mysqli_fetch_all($result, MYSQLI_ASSOC);

// получаем всё из таблицы sizes
$query = "
		SELECT *
		FROM `sizes`;        
	";
$result = mysqli_query($connect, $query);
$sizes = mysqli_fetch_all($result, MYSQLI_ASSOC);

// получаем всё из таблицы categories
$query = "
		SELECT *
		FROM `categories`;        
	";
$result = mysqli_query($connect, $query);
$categories = mysqli_fetch_all($result, MYSQLI_ASSOC);

// получаем все данные по конкретному букету
function getBouquetByID($id){
    $connect = getConnection();
    $query = "
		SELECT *
		FROM `bouquets`
        WHERE `bouquet_id` = $id;        
	";
    $result = mysqli_query($connect, $query);
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}
//echo "<pre>";
//print_r($bouquets);
//echo "</pre>";
//


?>