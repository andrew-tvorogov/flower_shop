<?php
include_once('globals.php'); // в globals лежит значние customer_id
include_once('functions.php');
include_once('florists.php'); // флористы из bd

include_once('./templates/header.html');
include_once('./templates/login_modal.html');
include_once('./templates/header_logo.html');
include_once('./templates/navigation.html');

if (isset($_GET['id'])){

    $id = $_GET['id'];

    $connect = getConnection();
    $query = "
		SELECT `florist_name`, `florist_img`, `florist_descr`, `bouquet_name`, `bouquet_img`, `bouquet_descr`
		FROM `florists`
		LEFT JOIN `bouquets` ON `florist_id` = `bouquet_florist_id`
        WHERE `florist_id` = $id;
	";
    $result = mysqli_query($connect, $query);
    $florist = mysqli_fetch_all($result, MYSQLI_ASSOC);



    include_once('./templates/florist.html');

}

include_once('./templates/footer.html');

