<?php

include_once('globals.php'); //
include_once('functions.php');
include_once('bouquets.php'); // букеты из bd
include_once('florists.php'); // флористы из bd

if (isAdmin()) {

    // список имён файлов изображений получен в bouquets.php

    // проверяем пришли ли данные формы, если пришли - идём на страницу admin-bouquets
    if (isset($_POST['new_name'])){
        $id = htmlentities($_POST['new_id']);
        // изменяем инфу
        $bouquet_new_name = htmlentities($_POST['new_name']);
        $bouquet_new_price = htmlentities($_POST['new_price']);
        $bouquet_new_image = htmlentities($_POST['new_image']);
        $bouquet_new_description = htmlentities($_POST['new_description']);
        $bouquet_new_florist_id = htmlentities($_POST['new_florist_id']);
        $bouquet_new_color_id = htmlentities($_POST['new_color_id']);
        $bouquet_new_size_id = htmlentities($_POST['new_size_id']);
        $bouquet_new_category_id = htmlentities($_POST['new_category_id']);
        $db = getConnection();
        $query = "UPDATE `bouquets` SET
                    `bouquet_name` = '$bouquet_new_name',
                    `bouquet_price` = '$bouquet_new_price',
                    `bouquet_img` = '$bouquet_new_image',
                    `bouquet_descr` = '$bouquet_new_description',
                    `bouquet_florist_id` = '$bouquet_new_florist_id',
                    `bouquet_color_id` = '$bouquet_new_color_id',
                    `bouquet_size_id` = '$bouquet_new_size_id',
                    `bouquet_category_id` = '$bouquet_new_category_id'                    
                    WHERE `bouquet_id` = '$id';
                    ";
        //echo $query;
        //die;
        mysqli_query($db, $query);
        header('Location: admin.php');
    }

    if (isset($_GET['id'])) {
        $id =  htmlentities($_GET['id']);
        $bouquetForEdit = getBouquetByID($id);
//        echo "<pre>";
//        print_r($bouquetForEdit);
//        echo "</pre>";
        include_once('./templates/header.html');
        include_once('./templates/header_logo.html');
        include_once('./templates/navigation.html');
        include_once('./templates/bouquet_edit.html');
        include_once('./templates/footer.html');
    }


} else {
    //error_reporting(0);
    header("Location: index.php");
}
?>