<?php

function getConnection()
{
    $connect = mysqli_connect(BDHOST, BDUSER, BDPASS, BDNAME);
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    } else {
        mysqli_set_charset($connect, 'utf8');
        return $connect;
    }
}

function generateToken($size = 32)
{
    $symbols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'];
    $symbolsLength = count($symbols);
    $token = '';
    for ($i = 0; $i < $size; $i++) {
        $token .= $symbols[rand(0, $symbolsLength - 1)];
    }
    return $token;
}

function checkIfEmailExists($email){
    $db = getConnection();
    $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`
        WHERE `user_email` = '$email';
        ";
    $result = mysqli_query($db, $query);
    $count = mysqli_fetch_assoc($result)['count'];
    return ($count === '1');
}

function checkIfUserExists($email, $password){
    $db = getConnection();
    $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`
        WHERE `user_email` = '$email' AND `user_password` = '$password';
        ";
    $result = mysqli_query($db, $query);
    $count = mysqli_fetch_assoc($result)['count'];
    return ($count === '1');
}

// TODO: возможно не будет использована
function getUserId ($email, $password){
    $db = getConnection();
    $query = "SELECT * 
                  FROM `users` 
                  WHERE `user_email` = '$email' AND
                        `user_password` = '$password';
                        ";
    $result = mysqli_query($db, $query);
    return mysqli_fetch_assoc($result)['user_id'];
}

function isAuthorized()
{
    $isAuthorized = false;
    if (isset($_COOKIE['user_id']) and isset($_COOKIE['token'])) {
        $user_id = $_COOKIE['user_id'];
        $token = $_COOKIE['token'];
        $db = getConnection();
        $query = "
				SELECT `connect_id`, UNIX_TIMESTAMP(`connect_token_time`) AS `token_time`
				FROM `connects`
				WHERE `connect_user_id` = '$user_id'
				AND `connect_token` = '$token';
			";
        $result = mysqli_query($db, $query);
        $connect_info = mysqli_fetch_assoc($result);
        if ($connect_info) {
            $isAuthorized = true;
            if (time() > $connect_info['token_time']) {
                $new_token = generateToken();
                $new_token_time = time() + 15 * 60;
                $connect_id = $connect_info['connect_id'];
                $query = "
						UPDATE `connects`
						SET `connect_token` = '$new_token',
							`connect_token_time` = FROM_UNIXTIME($new_token_time)
						WHERE `connect_id` = $connect_id;
					";
                //echo $query;
                mysqli_query($db, $query);
                setcookie('token', $new_token, time() + 2 * 24 * 3600, '/');
            }
        }
    }
    return $isAuthorized;
}

function isAdmin() {
    $isAdmin = false;
    if (isset($_COOKIE['user_id']) && isset($_COOKIE['token']) && isset($_COOKIE['token_time'])) {
        $userId = $_COOKIE['user_id']; // userId из cookie
        $db = getConnection();
        $query = "SELECT *
                        FROM `users`
                       WHERE `user_is_admin` = 1 
                         AND `user_id` = '$userId';
                   ";
        $result = mysqli_query($db, $query);
        $userIsAdminFlagExist =  mysqli_fetch_assoc($result);
        if ($userIsAdminFlagExist) {
            $isAdmin = true;
        }
    }
    return $isAdmin;
}

?>