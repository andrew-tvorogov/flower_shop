<?php

    $connect = getConnection();
    $query = "
		SELECT `order_id`, `delivery_name` AS `order_delivery_name`, `status_name` AS `order_status_name`, 
		       `user_name` AS `order_reguser_name`, `user_id`, 
		       `order_user_name`, `order_user_phone`, `order_delivery_address`, `order_delivery_comment`,
		       `order_start_time`, `order_finish_time`, `user_phone` as `order_reguser_phone`
		FROM `orders`
        LEFT JOIN `deliveries` ON `delivery_id` = `order_delivery_id`
        LEFT JOIN `users` ON `order_user_id` = `user_id`
        LEFT JOIN `statuses` ON `order_status_id` = `status_id` 
        WHERE `user_is_deleted` = 0
        ORDER BY `order_id`;
	";
    $result = mysqli_query($connect, $query);
    $orders = mysqli_fetch_all($result, MYSQLI_ASSOC);

    function getOrderSets($order){
        $connect = getConnection();
        $query = "
		SELECT *
		FROM `sets`                         
        WHERE `set_order_id` = $order;
	";
        $result = mysqli_query($connect, $query);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

?>
