<?php

include_once ('globals.php');
include_once ('functions.php');

// принимает строчку с предполагаемым email, имя параметра: email_exist_check
if(isset($_GET['email_exist_check'])){

    // получили email
    $email = htmlentities($_GET['email_exist_check']);
    // вернули 0 если нет такого email или 1, если уже есть
    if (checkIfEmailExists($email)) {
        echo '1';
        return true;
    } else {
        echo '0';
        return false;
    }
}

?>
