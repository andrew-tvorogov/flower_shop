<?php
include_once ('globals.php');
include_once ('functions.php');

// если получили bouquet_id и user_id
if(isset($_GET['bouquet_id']) && isset($_GET['user_id'])){

    $bouquet_id = htmlentities($_GET['bouquet_id']);
    $user_id = htmlentities($_GET['user_id']);

    if (checkIfAlreadyInFavorites($bouquet_id, $user_id)) {
        // да, в favorites - значит удаляем из favorites
        $db = getConnection();
        $query = "
		DELETE FROM `favorites`
        WHERE `favorite_bouquet_id` = $bouquet_id AND
            `favorite_user_id` = $user_id;
	";
        mysqli_query($db, $query);
        // удалили, возвращаем 0 для xhr
        // TODO: реализовать в js анимацию сердечка favorites
        echo '0';
    } else {
        // нет, не в favorites - значит добавляем в favorites
        $db = getConnection();
        $query = "
		INSERT INTO `favorites`
        SET `favorite_bouquet_id` = $bouquet_id,
            `favorite_user_id` = $user_id;
	";
        mysqli_query($db, $query);
        // добавили, возвращаем 1 для xhr
        // TODO: реализовать в js анимацию сердечка favorites
        echo '1';
    }
}

// информация о favorites для кабинета
function getFavorites($userId){
    $db = getConnection();
    $query = "
		SELECT *
		FROM `favorites`
		LEFT JOIN `bouquets` ON `bouquet_id` = `favorite_bouquet_id`
        WHERE `favorite_user_id` = $userId;        
	";
    $result = mysqli_query($db, $query);
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

function checkIfAlreadyInFavorites($bouquetId, $userId){
    $db = getConnection();
    $query = "
		SELECT *
		FROM `favorites`
        WHERE `favorite_bouquet_id` = $bouquetId AND `favorite_user_id` = $userId;        
	";
    $result = mysqli_query($db, $query);
    return mysqli_fetch_all($result, MYSQLI_ASSOC);
}

?>
