'use strict';

const gulp = require('gulp'),
    rimraf = require('rimraf'),
    scss = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    prefixer = require('gulp-autoprefixer'),
    htmlmin = require('gulp-htmlmin'),
    browserSync = require('browser-sync'),
    plumber = require('gulp-plumber'),
    rigger = require('gulp-rigger'),
    reload = browserSync.reload;

var path = {
    build:{
        all:'build/',
        html:'build/templates/',
		php:'build/',
        scss:'build/css/',
        js:'build/js/',
        img:'build/img/',
        font:'build/fonts/',
        favicon:'build/favicon/'
    },
    src:{
        html:'src/templates/**/*.{html,htm,json}',
		php:'src/*.{php,html}',
        scss:'src/scss/style.scss',
        js:'src/js/main.js',
        js_plugins:'src/js/plugins/*.js',
        img:'src/img/**/*.{jpg,gif,png,svg,jpeg,mp4}',
        font:'src/fonts/*.*',
        favicon:'src/favicon/*.*'
    },
    watch:{
		html:'src/templates/**/*.{html,htm}',
        php:'src/*.php',
        scss:'src/scss/**/*.scss',
        js:'src/js/**/*.js',        
        img:'src/img/**/*.{jpg,gif,png,svg,jpeg,mp4}'
    },
    clean:'build/'
},
//     config={
//     server:{
//         baseDir:"./build",
//         index:"index.html"
//     },
//         host:"localhost",
//         port:7787,
//         tunnel:true,
//         logPrefix:"Flower_shop"
// };
config = {
    proxy: "localhost/flower_shop/build/",
    notify: false
};

// чистим авгиевы конюшни
gulp.task('clean',function (done) {
    rimraf(path.clean,done);
});

// запуск webserver
gulp.task('webserver',function (done) {
    browserSync(config);
    done();
});

/* всё что относится к заданиям dev */
// работаем с html
gulp.task('dev:html',function (done) {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream:true}));
    done();
});
// работаем с php
gulp.task('dev:php',function (done) {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php))
        .pipe(reload({stream:true}));
    done();
});
// работаем с scss
gulp.task('dev:scss', function (done) {
    gulp.src(path.src.scss,{sourcemaps: true})
        .pipe(plumber()) // помог найти проблему
        .pipe(scss({            
            //outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.build.scss,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
    done();
});
// работаем с js
gulp.task('dev:js', function (done) {
    gulp.src(path.src.js,{sourcemaps: true})
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js,{sourcemaps:'.'}))
        .pipe(reload({stream:true}));
    done();
});

/* все задачи для prod */
// сжимаем html
gulp.task('prod:html',function (done) {
    gulp.src(path.src.html)
        .pipe(htmlmin({
            collapseWhitespace:true
        }))
        .pipe(gulp.dest(path.build.html));
    done();
});
// переносим php
gulp.task('prod:php',function (done) {
    gulp.src(path.src.php)
        .pipe(gulp.dest(path.build.php));
    done();
});
// сжимаем css, добавляем префиксы
gulp.task('prod:scss', function (done) {
    gulp.src(path.src.scss)
        .pipe(scss({            
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(prefixer({
            cascade:false,
            remove:true
        }))
        .pipe(gulp.dest(path.build.scss));
    done();
});
// сжимаем js
gulp.task('prod:js', function (done) {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
    done();
});
// перемещаем картинки в build
gulp.task('mv:img', function (done) {
    gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
    done();
});
// перемещаем шрифты в build
gulp.task('mv:font', function (done) {
    gulp.src(path.src.font)
        .pipe(gulp.dest(path.build.font));
    done();
});
// копируем js плагины в build, сжимаем
gulp.task('js:addons', function (done){
    gulp.src(path.src.js_plugins)
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js));
    done();
});
// копируем css плагины в build, сжимаем
gulp.task('css:addons', function (done){
    gulp.src('src/css/*.css')
        .pipe(scss({
            outputStyle:"compressed",
            sourcemaps:false
        }))
        .pipe(gulp.dest('build/css/'));
    done();
});
// перемещаем favicon
gulp.task('mv:favicon', function (done) {
    gulp.src(path.src.favicon)
        .pipe(gulp.dest(path.build.favicon));
    done();
});


// следим за изменениями в директориях
gulp.task('watch', function (done) {
    gulp.watch(path.watch.scss,gulp.series('dev:scss'));
    gulp.watch(path.build.scss,reload({stream:true}));
    gulp.watch(path.watch.html,gulp.series('dev:html'),reload({stream:true}));
	gulp.watch(path.watch.php,gulp.series('dev:php'),reload({stream:true}));
    gulp.watch(path.watch.js,gulp.series('dev:js'),reload({stream:true}));
    done();
});

//gulp.task('prod',gulp.series('clean',gulp.parallel('css:addons','mv:img','prod:html','prod:scss','prod:js'),'webserver'));
gulp.task('dev', gulp.series('clean', gulp.parallel('dev:scss', 'css:addons', 'mv:img', 'mv:favicon', 'mv:font', 'js:addons','dev:html','dev:php','dev:js'),'webserver','watch'));
//gulp.task('dev',gulp.series('clean',gulp.parallel('css:addons','mv:img', 'mv:favicon', 'mv:font', 'js:addons','dev:html','dev:php','dev:scss','dev:js'),'watch'));

gulp.task('default',gulp.series('dev'));